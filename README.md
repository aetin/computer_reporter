# AntLas reporter
Extracts data on a computer to understand its potential issues

### Install
For debian based distributions.
Using dpkg or the software center.

### What it does
Gather information from the computer regarding space usage,
RAM usage, network configuration, systemd logs,..
Creates an archive of all gathered info.
Warning : as of now, french only.