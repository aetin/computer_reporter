#!/bin/bash

######################################
# brief:  script meant to gather
# informations on local computer and 
# compress all in a file
#
# author : antlas@protonmail.com
#####################################

source antlas_report_utils.sh

CONF_ZENITY_WIDTH=300
CONF_ZENITY_HEIGHT=100

function sscreen()
{
	zenity	--info \
			--title="AntLas Reporter" \
           	--text "Bienvenue dans $APP_NAME, petit outil pour statuer sur l'état du pc." \
           	--width $CONF_ZENITY_WIDTH \
           	--height $CONF_ZENITY_HEIGHT
}

function run()
{
	gather_and_write_data |  zenity --progress \
		--title="AntLas Reporter : Progression" \
		--text="Collecte des données" \
		--no-cancel \
		--percentage=0 \
		--width $CONF_ZENITY_WIDTH \
		--height $CONF_ZENITY_HEIGHT \
		--auto-close \
		--auto-kill
		}

function post_process()
{
	[[ ! -e "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME" ]]\
		&& return 1

	zenity	--info \
		--title="AntLas Reporter" \
		--text "L'archive générée est disponible:\n$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME" \
		--width $CONF_ZENITY_WIDTH \
		--height $CONF_ZENITY_HEIGHT
}

function post_action()
{
	[[ ! -e "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME" ]]\
		&& return 1

	local choice=$(zenity --list \
		--title="AntLas Reporter" \
		--width 400 \
		--height 200 \
		--text "Que faire avec l'archive générée ?" \
		--radiolist \
		--column "Select" \
		--column "Choix" \
		FALSE "Ouvrir l'archive" \
		FALSE "Copier le contenu de l'archive" \
		FALSE "Copier le chemin de l'archive" \
		FALSE "Envoyer par mail (avec Thunderbird)" \
		FALSE "Ne rien faire")

	local matched=$(echo "$choice" | grep -Eo "Thunderbird")
	[[ -n "$matched" ]]\
		&& sudo -i -u "$SUDO_USER" thunderbird -compose "attachment='$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME'"

	matched=$(echo "$choice" | grep -Eo "chemin")
	[[ -n "$matched" ]]\
		&& echo "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME" | xclip \
		&& echo "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME" | xclip -selection c

	matched=$(echo "$choice" | grep -Eo "Ouvrir")
	[[ -n "$matched" ]]\
		&& xdg-open "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME"

	matched=$(echo "$choice" | grep -Eo "contenu")
	if [ -n "$matched" ];then
		local tpath=$(zenity --file-selection --directory)
		[[ -n "$tpath" ]]\
			&& cp "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME" "$tpath"\
			&& zenity --info \
			--title="AntLas Reporter" \
			--text "L'archive a bien été copiée dans $tpath" \
			--width $CONF_ZENITY_WIDTH \
			--height $CONF_ZENITY_HEIGHT
	fi
}

function antlas_reporter()
{
	sscreen
	run
	set_rights
	post_process
	post_action
}

# main
antlas_reporter