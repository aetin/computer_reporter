#!/bin/bash

######################################
# brief:  script meant to gather
# informations on local computer and 
# compress all in a file
#
# author : antlas@protonmail.com
#####################################

## GENERAL
APP_NAME="ANTLAS_REPORTER"
ARCHIVE_PREFIX_NAME="antlas_report"
ddate=$(date +%Y-%m-%d-%H_%M_%S)

## COMMANDS
CMD_GET_DISTRO_INFO="lsb_release -a"
CMD_GET_KERNEL_INFO="uname -a"
CMD_WHOAMI="whoami"
CMD_WHO="who"
CMD_W="w"
CMD_SPACE_USAGE="df -h"
CMD_UPTIME="uptime"
CMD_GET_RAM="free -h"
CMD_GET_TOP="top -bn 1"
CMD_GET_PROCESSES="ps aux"
CMD_GET_JOURNALCTL="journalctl -x --since=yesterday --until=now"
CMD_GET_SYSTEMD_UNITS="systemctl list-units"
CMD_GET_SYSTEMD_TIMERS="systemctl list-timers"
CMD_GET_SYSTEMD_SOCKETS="systemctl list-sockets --all"
CMD_GET_SYSTEMD_FILES="systemctl list-unit-files"
CMD_GET_BLE_DEV="hcitool dev"
CMD_GET_HOSTS="cat /etc/hosts"
CMD_GET_NETWMANAGER_STATUS="nmcli d"
CMD_NETWORK_STATUS="ip a"
CMD_NETWORK_LINK="ip l"
CMD_GET_ROUTE="ip route"
CMD_GET_IPTABLES="iptables -L"
CMD_GET_NETW_CONNECTIONS="ss -tulpn"
CMD_GET_AVAHI_DISC="avahi-browse -arlt"
CMD_GET_LISTENING_PROCESS="lsof -iTCP -sTCP:LISTEN"
CMD_GET_CONNECTED_PROCESS="lsof -iTCP -sTCP:ESTABLISHED"

## FORMATTING
FORMAT_SEPARATOR="##################################"

## DIRS & FILES

DIR_OUTPUT=$(mktemp -d)
DIR_GEN_FILES_SUBDIR="data_$ddate"
DIR_ARCHIVE_CREATION="$DIR_OUTPUT"
DIR_GEN_FILES="$DIR_OUTPUT/$DIR_GEN_FILES_SUBDIR"

FILE_GENERAL="general.dat"
FILE_ERRORS_LOGS="logs.dat"
FILE_NETW_STATUS="network_status.dat"
FILE_NETW_CONN="network_connections.dat"
FILE_CONF="configurations.dat"

PATH_FILE_GENERAL="$DIR_GEN_FILES/general.dat"
PATH_FILE_ERRORS_LOGS="$DIR_GEN_FILES/logs.dat"
PATH_FILE_NETW_STATUS="$DIR_GEN_FILES/network_status.dat"
PATH_FILE_NETW_CONN="$DIR_GEN_FILES/network_connections.dat"
PATH_FILE_CONF="$DIR_GEN_FILES/configurations.dat"

## archive name
ARCHIVE_NAME="$ARCHIVE_PREFIX_NAME-$ddate.tar.gz"

function set_rights()
{
	chmod +rx "$DIR_ARCHIVE_CREATION"
	chmod +rx "$DIR_ARCHIVE_CREATION/$ARCHIVE_NAME"
}

function compress_files()
{
	cd "$DIR_ARCHIVE_CREATION"
	tar czvf "$ARCHIVE_NAME"  "$DIR_GEN_FILES_SUBDIR/$FILE_GENERAL" "$DIR_GEN_FILES_SUBDIR/$FILE_ERRORS_LOGS" "$DIR_GEN_FILES_SUBDIR/$FILE_NETW_STATUS" "$DIR_GEN_FILES_SUBDIR/$FILE_NETW_CONN" "$DIR_GEN_FILES_SUBDIR/$FILE_CONF" 2> /dev/null
}

function print_header()
{
	local ddate=$(date)
	echo "$FORMAT_SEPARATOR"
	echo "# Report gathered by $APP_NAME"
	echo "# $ddate"
	echo "$FORMAT_SEPARATOR"
}

function create_dirs()
{
	[[ ! -e "$DIR_GEN_FILES" ]]\
		&& mkdir -p "$DIR_GEN_FILES"
}

function execute_commands()
{
	[[ -z "$1" ]]\
		&& return 255

	declare -a array=("${!1}")

	print_header
	for i in "${array[@]}";do 
		echo "$FORMAT_SEPARATOR"
		echo "### $i"
		${i} 2> /dev/null
		echo "$FORMAT_SEPARATOR"
	done
}


function report_general_info()
{
	local cmds=( "$CMD_GET_DISTRO_INFO"\
		"$CMD_GET_KERNEL_INFO"\
		"$CMD_UPTIME"\
		"$CMD_WHOAMI"\
		"$CMD_WHO"\
		"$CMD_W"\
		"$CMD_SPACE_USAGE"\
		"$CMD_GET_RAM"\
		"$CMD_GET_TOP"\
		"$CMD_GET_PROCESSES"\
	)

	execute_commands cmds[@]
}

function report_errors_logs()
{
	local cmds=(
		"$CMD_GET_JOURNALCTL"
	)

	execute_commands cmds[@]
}

function report_network_status()
{
	local cmds=(
		"$CMD_GET_BLE_DEV"\
		"$CMD_NETWORK_STATUS"\
		"$CMD_NETWORK_LINK"\
		"$CMD_GET_NETWMANAGER_STATUS"\
		"$CMD_GET_ROUTE"\
		"$CMD_GET_IPTABLES"
	)

	execute_commands cmds[@]
}

function report_network_connections()
{
	local cmds=(
		"$CMD_GET_NETW_CONNECTIONS"\
		"$CMD_GET_LISTENING_PROCESS"\
		"$CMD_GET_CONNECTED_PROCESS"\
		"$CMD_GET_AVAHI_DISC"
	)

	execute_commands cmds[@]
}

function report_get_configurations()
{
	local cmds=(
		"$CMD_GET_HOSTS"\
		"$CMD_GET_SYSTEMD_UNITS"\
		"$CMD_GET_SYSTEMD_TIMERS"\
		"$CMD_GET_SYSTEMD_SOCKETS"\
		"$CMD_GET_SYSTEMD_FILES"
	)

	execute_commands cmds[@]
}

function gather_and_write_data()
{
	create_dirs

	echo "15"
	report_general_info > "$PATH_FILE_GENERAL"

	echo "30"
	report_errors_logs > "$PATH_FILE_ERRORS_LOGS"

	echo "45"
	report_get_configurations > "$PATH_FILE_CONF"

	echo "60"
	report_network_connections > "$PATH_FILE_NETW_CONN"

	echo "75"
	report_network_status > "$PATH_FILE_NETW_STATUS"

	echo "90"
	compress_files

	echo "100"
}
